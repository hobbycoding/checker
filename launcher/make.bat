md .\target\32bit
md .\target\64bit

copy /Y .\winrun4j\bin\winrun4j.exe .\target\32bit\checker.exe
.\winrun4j\bin\rcedit /C .\target\32bit\checker.exe
.\winrun4j\bin\rcedit /I .\target\32bit\checker.exe .\src\main\resources\checker.ico
.\winrun4j\bin\rcedit /N .\target\32bit\checker.exe .\src\main\ini\checker.ini
rem .\winrun4j\bin\rcedit /S .\target\32bit\taskman.exe .\src\main\resources\taskman_splash.bmp

copy /Y .\winrun4j\bin\winrun4j64.exe .\target\64bit\checker.exe
.\winrun4j\bin\rcedit64 /C .\target\64bit\checker.exe
.\winrun4j\bin\rcedit64 /I .\target\64bit\checker.exe .\src\main\resources\checker.ico
.\winrun4j\bin\rcedit64 /N .\target\64bit\checker.exe .\src\main\ini\checker.ini
rem .\winrun4j\bin\rcedit64 /S .\target\64bit\taskman.exe .\src\main\resources\taskman_splash.bmp

copy .\src\main\ini\checker.ini .\target\checker.ini