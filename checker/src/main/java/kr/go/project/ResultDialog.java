package kr.go.project;

import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.json.simple.JSONObject;

import static kr.go.project.Main.MAIN_FXML;
import static kr.go.project.Main.RESULT_FXML;
import static kr.go.project.util.FXUtil.getStage;

public class ResultDialog {
    private static Stage dialog;

    public ResultDialog(ObservableList<JSONObject> list) {
        if (dialog == null) {
            dialog = getStage(RESULT_FXML);
            dialog.getScene().getStylesheets().add("/kr/go/project/ui/checker.css");
            dialog.getIcons().add(new Image("/images/dose.ico"));
            dialog.initStyle(StageStyle.DECORATED);
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.initOwner(getStage(MAIN_FXML));
        }
        ResultController.getInstance().setTabPane(list);
    }

    public void show() {
        dialog.showAndWait();
    }
}

