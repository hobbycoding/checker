package kr.go.project;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import kr.go.project.util.FXUtil;

import java.net.URL;

public class Main extends Application {
    public final static String MAIN_FXML = "ui/main.fxml";
    public final static String RESULT_FXML = "ui/result.fxml";

    @Override
    public void start(Stage primaryStage) throws Exception {
        Class object = getClass();
        URL url = object.getResource(MAIN_FXML);
        Parent root = FXMLLoader.load(url);
        primaryStage.setTitle("Checker");
        primaryStage.getIcons().add(new Image("/images/pills.ico"));
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
