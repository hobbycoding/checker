package kr.go.project;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.print.PageLayout;
import javafx.print.PrinterJob;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import kr.go.project.util.FXUtil;
import org.controlsfx.control.PopOver;
import org.fxmisc.richtext.InlineCssTextArea;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import static kr.go.project.Main.RESULT_FXML;

public class ResultController implements Initializable {
    public Button print;
    public AnchorPane resultPane;
    public TabPane tabPane;
    public Button btn1;
    public Button btn2;
    public Button btn3;
    public Button btn4;
    private ObservableList<JSONObject> selectedList;
    private JSONObject buttonData;

    public static final Logger logger = Logger.getLogger("ResultController");
    private static ResultController instance;
    private String buttonDataName = "buttonData.txt";

    public static ResultController getInstance() {
        return instance;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (instance == null)
            instance = this;
        setButton(btn1);
        setButton(btn2);
        setButton(btn3);
        setButton(btn4);
    }

    public void setTabPane(ObservableList<JSONObject> list) {
        // make tabItem.
        setAccordion(list);
        selectedList = list;
        tabPane.prefWidthProperty().bind(tabPane.widthProperty().divide(4));
        tabPane.getSelectionModel().selectLast();
    }

    public void print() {
        PrinterJob job = PrinterJob.createPrinterJob();
        if (job != null && job.showPrintDialog(FXUtil.getStage(RESULT_FXML).getOwner())) {
            selectedList.stream().filter(object -> object.get("flag").toString().equalsIgnoreCase("true")).forEach(object -> {
                PageLayout pageLayout = job.getJobSettings().getPageLayout();
                ImageView image = FXUtil.getImages(object.get("name").toString());
                image.setPreserveRatio(true);
                image.setFitHeight(pageLayout.getPrintableHeight());
                image.setFitWidth(pageLayout.getPrintableWidth());
                job.printPage(image);
            });
            job.endJob();
        }
    }

    private void setButton(Button btn) {
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                PopOver popOver = new PopOver();
                popOver.setContentNode(getBtnContent(btn));
                popOver.show(btn);
            }
        });
    }

    private Node getBtnContent(Button btn) {
        try {
            parseBtnData();
            AnchorPane anchorPane = new AnchorPane();
            Label label = new Label(buttonData.get(btn.getId()).toString());
            setPadding(anchorPane, label, 10.0);
            anchorPane.getChildren().add(label);
            return anchorPane;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setAccordion(ObservableList<JSONObject> list) {
        tabPane.getTabs().clear();
        list.stream().filter(object -> (boolean) object.get("flag")).forEach(object -> {
            tabPane.getTabs().add(getTab(object));
            logger.info("add tab : " + object.get("name"));
        });
    }

    private Tab getTab(JSONObject object) {
        Tab tab = new Tab(object.get("name").toString());
        VBox accordion = new VBox();
        ObservableList<TableColumn> tableViewList = MainController.getInstance().tableView.getColumns();
        for (int i = 0; i < tableViewList.size(); i++) {
            String colName = tableViewList.get(i).textProperty().getValue();
            if (!colName.isEmpty() && !colName.equals("name")) {
                TitledPane pane = new TitledPane(colName, getContent(object.get(colName)));
                pane.setExpanded(false);
                accordion.getChildren().add(pane);
            }
        }
        tab.setContent(accordion);
        tabPane.getSelectionModel().select(tab);
        return tab;
    }

    private AnchorPane getContent(Object value) {
        AnchorPane anchorPane = new AnchorPane();
        anchorPane.setStyle("-fx-padding:1;");
        InlineCssTextArea area = new InlineCssTextArea();
        if (value instanceof JSONArray) {
            int cursor, textLength;
            String text;
            for (Object object : (JSONArray)value) {
                cursor = area.getLength();
                if (object instanceof JSONObject) {
                    text = ((JSONObject)object).get("value").toString();
                } else text = object.toString();
                textLength = ("- " + text + "\n").length();
                area.appendText("- " + text + "\n");
                if (object instanceof JSONObject && ((JSONObject) object).containsKey("style"))
                    area.setStyle(cursor, textLength + cursor, FXUtil.getCSSStyle(object));
            }
        } else {
            area.appendText(value.toString());
        }
        setPadding(anchorPane, area, 10.0);
        anchorPane.getChildren().add(area);
        return anchorPane;
    }

    private void setPadding(AnchorPane pane, Node node, double v) {
        pane.setTopAnchor(node, v);
        pane.setLeftAnchor(node, v);
        pane.setRightAnchor(node, v);
        pane.setBottomAnchor(node, v);
    }

    private void parseBtnData() throws Exception {
        if (buttonData == null || buttonData.isEmpty()) {
            JSONParser parser = new JSONParser();
            InputStream stream = Class.class.getResourceAsStream("/" + buttonDataName);
            buttonData = (JSONObject) parser.parse(new InputStreamReader(stream, "utf-8"));
        }
    }
}

