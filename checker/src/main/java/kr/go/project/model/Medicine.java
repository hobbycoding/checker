package kr.go.project.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * make Model, but not used.
 */
public class Medicine {
    private final StringProperty name = new SimpleStringProperty("Name");
    private final StringProperty sideEffect = new SimpleStringProperty("SideEffect");

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public String getSideEffect() {
        return sideEffect.get();
    }

    public StringProperty sideEffectProperty() {
        return sideEffect;
    }
}
