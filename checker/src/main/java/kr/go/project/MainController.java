package kr.go.project;

import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import kr.go.project.extend.CustomCell;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Logger;

public class MainController implements Initializable {

    public TableView tableView;
    public TableColumn<JSONObject, Boolean> check;
    public TableColumn<JSONObject, String> name;
    public TableColumn<JSONObject, String> adverseDrug;
    public TableColumn<JSONObject, String> alertSx_Sn;
    public TableColumn<JSONObject, String> test;
    public TableColumn<JSONObject, String> drugInteraction;
    public TableColumn<JSONObject, String> etc1;
    public TableColumn<JSONObject, String> etc2;
    public Button search;

    public static final Logger logger = Logger.getLogger("MainController");
    private static boolean init = false;
    private static MainController instance;
    private ObservableList<JSONObject> dataList;
    private String defaultPath = "/";
    private String defaultName = "data.txt";
    private JSONArray arrayData;

    public static MainController getInstance() {
        return instance;
    }

    public void initialize(URL location, ResourceBundle resources) {
        try {
            if (!init) {
                logger.info("Initialize");
                readFile();
                setTableView();
                init = true;
                instance = this;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchButtonClick() {
        ResultDialog dialog = new ResultDialog(dataList);
        dialog.show();
    }

    private void readFile() throws Exception {
        JSONParser parser = new JSONParser();
        InputStream stream = Class.class.getResourceAsStream(defaultPath + defaultName);
        arrayData = (JSONArray) parser.parse(new InputStreamReader(stream, "utf-8"));

        //set table Data. & set Edit.(use checkbox)
        for (Object object : arrayData)
            ((JSONObject) object).put("flag", false);
        dataList = FXCollections.observableArrayList(arrayData);
        tableView.setItems(dataList);
        tableView.setEditable(true);
    }

    private void setTableView() {
        //set Table Cell
        check.setCellFactory(param -> {
            CustomCell cell = new CustomCell<>();
            cell.getCheckBox().setOnAction(event ->
                    dataList.get(cell.getIndex()).put("flag", ((CheckBox)event.getSource()).isSelected()));
            return cell;
        });
        check.setCellValueFactory(param -> {
            Object object = param.getValue();
            return new ReadOnlyBooleanWrapper(Boolean.valueOf(((JSONObject) object).get("flag").toString()));
        });

        CheckBox cb = new CheckBox();
        cb.setUserData(check);
        cb.setOnAction(handleSelectAllCheckbox());
        check.setGraphic(cb);

        setCellValueFactory(name, false);
        setCellValueFactory(adverseDrug);
        setCellValueFactory(alertSx_Sn);
        setCellValueFactory(test);
        setCellValueFactory(drugInteraction);
        setCellValueFactory(etc1);
        setCellValueFactory(etc2);

        adverseDrug.prefWidthProperty().bind(tableView.widthProperty().divide(4));
        alertSx_Sn.prefWidthProperty().bind(tableView.widthProperty().divide(4));
        test.prefWidthProperty().bind(tableView.widthProperty().divide(2));
    }

    private void setCellValueFactory(TableColumn<JSONObject, String> col) {
        setCellValueFactory(col, true);
    }

    private void setCellValueFactory(TableColumn<JSONObject, String> col, boolean makeCheckbox) {
        String key = col.getText();
        col.setCellValueFactory(param -> {
            JSONObject object = param.getValue();
            String str = "";
            if ((object.get(key)) instanceof JSONArray)
                for(Object obj :((JSONArray)(object.get(key)))) {
                    if (obj instanceof JSONObject)
                        str += ((JSONObject) obj).get("value");
                    else str += obj.toString() + ", ";
                } else str = (object.get(key)).toString();
            return new ReadOnlyStringWrapper(str != null ? str : "");
        });
        if (makeCheckbox)
            col.setGraphic(new CheckBox());
    }

    private EventHandler<ActionEvent> handleSelectAllCheckbox() {
        return event -> {
            CheckBox cb = (CheckBox) event.getSource();
            updateCheckItems(cb.isSelected());
            refresh();
        };
    }

    private void refresh() {
        tableView.setItems(null);
        tableView.layout();
        tableView.setItems(FXCollections.observableList(dataList));
    }

    private void updateCheckItems(boolean flag) {
        for (Object obj : dataList)
            ((JSONObject)obj).put("flag", flag);
    }

    public ObservableList<JSONObject> getDataList() {
        return dataList;
    }
}
