package kr.go.project.util;

import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import kr.go.project.MainController;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.Map;
import java.util.logging.Logger;


public class FXUtil {
    public static final Logger logger = Logger.getLogger("FXUtil");
    public static Stage getStage(String path) {
        Stage stage = new Stage();
        try {
            URL location = MainController.class.getResource(path);
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(location);
            fxmlLoader.setBuilderFactory(new JavaFXBuilderFactory());
            Parent main = fxmlLoader.load();
            stage.setScene(new Scene(main));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stage;
    }

    // 1: 굵게, 2:밑줄, 3:글자포인트 +3, 4:파랑, 5:빨강
    // "style" :["font-weight" :"bold", "underline" : "true", "font-size" : "20", "font-color" : "red"]
    public static String getCSSStyle(Object obj) {
        if (obj instanceof String)
            return "";
        TextStyle style = new TextStyle();
        for (Object object : ((JSONObject)((JSONObject)obj).get("style")).entrySet()) {
            style = setStyle((Map.Entry<String, String>) object, style);
        }
        logger.info(style.toCss());
        return style.toCss();
    }

    public static TextStyle setStyle(Map.Entry<String, String> object, TextStyle style) {
        switch (object.getKey()) {
            case "font-weight" :
                if (object.getValue().equalsIgnoreCase("bold"))
                    return style.updateBold(true);
            case "underline" :
                if (object.getValue().equalsIgnoreCase("true"))
                    return style.updateUnderline(true);
            case "font-size" :
                return style.updateFontSize(Integer.parseInt(object.getValue()));
            case "font-color" :
                return style.updateTextColor(Color.web(object.getValue()));
        }
        return style;
    }

    public static ImageView getImages(String name) {
        Image image = new Image("/images/" + name + ".png");
        ImageView imageView = new ImageView(image);
        return imageView;
    }
}
